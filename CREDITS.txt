          	   	  		Promethee 2.0.0
		  see: https://gitlab.com/irsn/promethee2/-/wikis


I) Objects

   Promethee is mainly a front-end for Funz engine (http://doc.funz.sh/),
   dedicated to parametric computing. It merges an easy extensibilty for any
   scientific simulation software, and standard algorithms for design of
   computer experiments. It was designed, developped and supported for safety
   assessments, based on IRSN (https://www.irsn.fr) return of experiment.

II) People

  The following developers, designers, experts and engineers have been involved in
  the project:

    - As core software designers and developers:
      -- Mr Julien Forest, Artenum SARL
      -- Mr Benjamin Jeanty-Ruard, Artenum SARL
      -- Mr Arnaud Trouche, Artenum SARL
      
    - As core software designers:
      -- Mr Yann Richet, IRSN


