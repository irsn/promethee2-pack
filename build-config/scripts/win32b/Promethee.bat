@echo off

:: compute the directory where the command is launched (pwd equivalent)
SET STARTPOINT=%cd%
::echo "STARTPOINT"%STARTPOINT%

:: compute the name of the .batch script
set batchScriptName=%~nx0
::echo "batchScriptName"%batchScriptName%

:: compute the path of the .bat script
SET mypath=%~dp0
::echo "mypath"%mypath%

:: go to directory where is defined the path of the .bat script
pushd %mypath%

set NOW=%date:~-4,4%%date:~-7,2%%date:~-10,2%%FormatedTime:~-11,2%%FormatedTime:~-8,2%%FormatedTime:~-5,2%%FormatedTime:~-2,2%

dependencies\thirdparty\jre-${jre.version}-win32b\bin\java.exe -Xmx${app.heap.size}M -jar -Dlogback.configurationFile=resources/logging/logback.xml -Dpromethee.timestamp=%NOW% -Dapp.name=Promethee2 -Dfelix.config.properties=file:resources/felix/config.properties   -Dorg.keridwen.config=./resources/promethee-ui.properties -splash:resources/images/promethee-splash-screen.png dependencies\thirdparty\felix-framework-${felix.version}\bin\felix.jar 
