#!/bin/bash

START_POINT=$PWD
CMD_LINE=`dirname "$0"`
cd "${CMD_LINE}"
NOW=`date +%s`
./dependencies/thirdparty/jre-${jre.version}-osx64b/Contents/Home/bin/java -Xmx${app.heap.size}M -jar -Dlogback.configurationFile=resources/logging/logback.xml -Dpromethee.timestamp=$NOW -Dapp.name=Promethee2 -Dfelix.config.properties=file:resources/felix/config.properties -Dorg.keridwen.config=./resources/promethee-ui.properties -splash:resources/images/promethee-splash-screen.png dependencies/thirdparty/felix-framework-${felix.version}/bin/felix.jar 

