# Promethee packaging

This is the packaging repository of Promethee. From Promethee sources, it builds a package with basic plugins and tests everything works. All Promethee distribution repositories should extend this content to build an installable Promethee distribution.

Tasks:

* get Promethee sources from IRSN/Promethee repository
* compile and merge dependencies
* include basic plugins for testing
* creates .zip basic packages
* run Funz tests to check Run & RunDesign features are working
* run Snitch tests to test features on basic plugins
