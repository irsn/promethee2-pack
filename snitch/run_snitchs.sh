#!/bin/bash

EXIT=1

i=0
while [[ $EXIT != "0" ]]; do 
  ((i++));if [[ i -eq 5 ]];then break;fi
  kill -9 $(pidof java)
  mv $HOME/.Promethee2/projects projects.$i
  mv dump dump.$i
  mv log log.$i
  mv *.result.json result.$i.json
  /bin/bash /snicker/run_snitch.sh $*
  EXIT=$?
  echo $EXIT
done

exit $EXIT
