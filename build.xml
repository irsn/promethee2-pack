<project name="Promethee" basedir="." xmlns:artifact="antlib:org.apache.maven.artifact.ant">

	<!-- ############################################################## -->
	<!--                      Global properties                         -->
	<!-- ############################################################## -->

	<!-- Build dates -->
	<tstamp>
		<format property="year" pattern="yyyy" />
	</tstamp>
	<tstamp>
		<format property="build.time" pattern="yyyy-MM-dd_HH.mm.ss.SSS" />
	</tstamp>

	<!-- ############################################################## -->
	<!--                       Project structure                        -->
	<!-- ############################################################## -->

	<property environment="env"/>

	<!-- Build directories structure -->
	<property name="build" value="build" />

	<!-- Distribution directory -->
	<property name="distribution" value="packages" />
	<property name="multios.name" value="all" />

	<!-- Source directories -->
	<property name="build.config" value="build-config" />
	<property name="data" value="data" />
	<property name="dependencies.main" value="dependencies/main" />
	<property name="dependencies.thirdparty" value="dependencies/thirdparty" />
	<property name="documentation" value="documentation" />
	<property name="lib" value="lib" />
	<property name="plugins" value="plugins" />
	<property name="resources" value="resources" />
	<property name="samples" value="samples" />
	<property name="scripts" value="scripts" />

	<!-- ############################################################## -->
	<!--                     Ant contrib tasks                          -->
	<!-- ############################################################## -->

	<taskdef resource="net/sf/antcontrib/antlib.xml">
		<classpath>
			<pathelement location="${basedir}/dependencies/build/ant-contrib-1.0b3.jar" />
		</classpath>
	</taskdef>

	<!-- ############################################################## -->
	<!--                       Ant help message                         -->
	<!-- ############################################################## -->

	<target name="help" description="Display the main targets available">
		<echo message="########################################################################" />
		<echo message="Ant tasks available:" />
		<echo message=" " />
		<echo message=" - clean              : remove all Ant-generated files" />
		<echo message=" - quotas             : generate quotas.hex from quotas.xml" />
		<echo message=" - install            : create an executable version for Linux 64bits" />
		<echo message=" - package            : create the distribution packages for each" />
		<echo message="                        supported platform" />
		<echo message=" " />
		<echo message="########################################################################" />
	</target>

	<!-- ############################################################## -->
	<!--                    Cleaning Ant files                          -->
	<!-- ############################################################## -->

	<target name="clean" description="Clean Ant-generated files">
		<clean />
	</target>

	<macrodef name="clean">
		<element name="additional-clean" optional="true" implicit="true" />
		<sequential>
			<echo>###############################################################</echo>
			<echo>                  Cleaning Ant-generated files                 </echo>
			<echo>###############################################################</echo>
			<delete dir="${basedir}/${build}" />
			<delete dir="${basedir}/${distribution}" />
			<delete file="${basedir}/VERSION.txt" />
			<delete file="${basedir}/quotas.hex" />
			<delete file="${basedir}/yguardlog.xml" />
			<additional-clean />
			<echo />
		</sequential>
	</macrodef>

	<!-- ############################################################## -->
	<!--                Distributable packaging generation              -->
	<!-- ############################################################## -->

	<target name="v1base">
		<loadProperties name="ui-v1" />

		<createPrometheeReleaseName />

		<buildDistributionDirectories/>

		<copyBundles bundles="${bundles.list}" />

		<copyMainDependencies bundles="${dependencies.list}" />

		<copyPlugins plugins="${plugins.list}"/>

		<copyResources />

		<generateQuotas />

		<copyTextFiles name="" >
			<include name="Promethee2.conf" />
			<include name="About.html" />
			<include name="quotas.hex" />
		</copyTextFiles>

		<copyToThirdpartyDependencies dependency="felix-framework-${felix.version}" />

		<removeHiddenFiles />
	</target>

	<target name="install" description="Generate a distributable package for linux64b" depends="v1base">

		<createDistribution platform="linux64b" />
		<createDistribution platform="win32b" />
		<makeScriptsExecutable />

		<echo message="##############################################################" />
		<echo message="              Linux Installation completed"/>
		<echo message="##############################################################" />
	</target>

	<target name="package" description="Generate a distributable package for each supported platform"  depends="v1base">
		<createDistributions />

		<echo message="##############################################################" />
		<echo message="                  Packaging completed" />
		<echo message="##############################################################" />
	</target>

	<target name="quotas" description="Generates the quotas.hex file from the quotas.xml file">
		<loadProperties name="ui-v1" />
		<mkdir dir="${build}" />
		<generateQuotas/>
	</target>


	<!-- ############################################################## -->
	<!--       		    Update the exemple launch scripts	        -->
	<!-- ############################################################## -->

	<target name="updateLinuxLaunchScript" description="Update the exemple launch scripts for Linux64b">
		<echo>V1</echo>
		<antcall target="genericUpdateLinuxLaunchScript">
			<param name="flavour" value="ui-v1" />
		</antcall>
	</target>


	<target name="genericUpdateLinuxLaunchScript">

		<condition property="repository" value="${env.USER_HOME}/.m2/repository" else="${user.home}/.m2/repository">
			<isset property="env.USER_HOME" />
		</condition>

		<loadProperties name="${flavour}" />

		<!-- Set the location of the file to write using the script name property -->
		<property name="launch.script.file" value="${basedir}/${build.config}/scripts/linux64b/${launch.script.file.name}" />

		<!-- Erase the content and put the header -->
		<echo file="${launch.script.file}">user.home = file:/home/__USERNAME__/.m2/repository/</echo>
		<echo file="${launch.script.file}" append="true" />

		<!-- Count the number of jars inside the dependencies/main directory to determine the id of the first bundle to start -->
		<resourcecount property="count">
			<fileset dir="${basedir}/${dependencies.main}">
				<include name="**/*" />
			</fileset>
		</resourcecount>

		<!-- Define all used properties -->
		<property name="shortLine" value="" />
		<math result="newValue" operand1="${count}" operation="+" operand2="0" datatype="int" />
		<var name="count" value="${newValue}" />
		<property name="tempLine" value="" />
		<property name="startLine" value="start ${count}" />

		<!-- Externals libs -->
		<loadFileList property="external.libs" path="${basedir}/${build.config}/${flavour}.dependencies.list.txt" />
		<for param="line" list="${external.libs}" delimiter="${line.separator}">
			<sequential>
				<!-- Remove the /home/USERNAME/.m2/repository part to add genericity to the updated file -->
				<propertyregex property="shortLine" override="true" input="@{line}" regexp=".*/.m2/repository/(.*)" select="\1" />

				<!-- Add the install file line -->
				<echo file="${launch.script.file}" append="true">
install $user.home/${shortLine}</echo>

				<!-- Compute the new bundle id (count +1) -->
				<math result="newValue" operand1="${count}" operation="+" operand2="1" datatype="int" />
				<var name="count" value="${newValue}" />

				<!-- Append the bundle id to the 'start' line -->
				<var name="tempLine" value="${startLine} ${count}" />
				<var name="startLine" value="${tempLine}" />
			</sequential>
		</for>

		<!-- Bundles -->
		<loadFileList property="internal.bundles" path="${basedir}/${build.config}/${flavour}.bundles.list.txt" />
		<for param="line" list="${internal.bundles}" delimiter="${line.separator}">
			<sequential>

				<!-- Remove the /home/USERNAME/.m2/repository part to add genericity to the updated file -->
				<propertyregex property="shortLine" override="true" input="@{line}" regexp=".*/.m2/repository/(.*)" select="\1" />

				<!-- Add the install file line -->
				<echo file="${launch.script.file}" append="true">
install $user.home/${shortLine}</echo>

				<!-- Compute the new bundle id (count +1) -->
				<math result="newValue" operand1="${count}" operation="+" operand2="1" datatype="int" />
				<var name="count" value="${newValue}" />

				<!-- Append the bundle id to the 'start' line -->
				<var name="tempLine" value="${startLine} ${count}" />
				<var name="startLine" value="${tempLine}" />
			</sequential>
		</for>

		<!-- Write the start line to the end of the file -->
		<echo file="${launch.script.file}" append="true">
				
${startLine}
		</echo>
	</target>


	<!-- Generate a distributable package for each supported platform -->
	<target name="createDistributions" description="Generate a distributable package for each supported platform">

		<!-- Installing Promethee on each supported platforms -->
		<createDistribution platform="linux64b" />
		<createDistribution platform="osx64b" />
		<createDistribution platform="win32b" />
		<createDistribution platform="win64b" />

		<!-- Creating an all-OS distribution -->
		<echo>###############################################################</echo>
		<echo>               Creating multi-OS distribution                  </echo>
		<echo>###############################################################</echo>

		<mkdir dir="${distribution.dir}/multiOS" />

		<!-- Create a local variable that store the location of the distribution -->
		<local name="platform.dir" />
		<property name="platform.dir" value="${distribution.dir}/multiOS/${promethee.release.name}-all" />
		<mkdir dir="${platform.dir}" />

		<!-- Copies the cross-platform basis -->
		<copy todir="${platform.dir}">
			<fileset dir="${build.dir}/${promethee.release.name}" />
		</copy>


		<macrodef name="fillAllDistributionForPlatform">
			<attribute name="platform" />
			<sequential>

				<!-- Copies and configures the platform-specific execution script -->
				<copy todir="${platform.dir}">
					<filterchain>
						<expandproperties />
					</filterchain>
					<fileset dir="${build.config.dir}/scripts/@{platform}">
						<include name="*.sh" />
					</fileset>
					<mapper type="glob" from="*.sh" to="*-@{platform}.sh" />
				</copy>
				<copy todir="${platform.dir}">
					<filterchain>
						<expandproperties />
					</filterchain>
					<fileset dir="${build.config.dir}/scripts/@{platform}">
						<include name="*.bat" />
					</fileset>
					<mapper type="glob" from="*.bat" to="*-@{platform}.bat" />
				</copy>

				<!-- Copy previously unzipped JRE -->
				<copy todir="${platform.dir}/dependencies/thirdparty/jre-${jre.version}-@{platform}">
					<fileset dir="${distribution.dir}/@{platform}/${promethee.release.name}-@{platform}/dependencies/thirdparty/jre-${jre.version}-@{platform}">
						<include name="**/*" />
					</fileset>
				</copy>

				<!-- Now the JRE is copied, we can remove the original dir -->
				<delete dir="${distribution.dir}/@{platform}/${promethee.release.name}-@{platform}" />

			</sequential>
		</macrodef>

		<fillAllDistributionForPlatform platform="linux64b" />
		<fillAllDistributionForPlatform platform="osx64b" />
		<fillAllDistributionForPlatform platform="win32b" />
		<fillAllDistributionForPlatform platform="win64b" />

		<!-- ZIP distribution -->
		<zip destfile="${platform.dir}.zip" duplicate="preserve">
			<zipfileset dir="${platform.dir}" includes="**/*.sh" filemode="755" />
			<zipfileset dir="${platform.dir}" includes="**/*.bat" filemode="755" />
			<zipfileset dir="${platform.dir}" includes="**/*.exe" filemode="755" />
			<zipfileset dir="${platform.dir}" includes="**/*.dll" filemode="755" />
			<zipfileset dir="${platform.dir}" includes="**/java" filemode="755" />
			<zipfileset dir="${platform.dir}" includes="**/gmsh" filemode="755" />
			<zipfileset dir="${platform.dir}" includes="**/jspawnhelper" filemode="755" />
			<zipfileset dir="${platform.dir}" includes="**/*" />
		</zip>
		<delete dir="${platform.dir}" />

	</target>


	<!-- ############################################################## -->
	<!--               			 Installation macros            		-->
	<!-- ############################################################## -->

	<!-- Load properties where are defined, for example, the qualifier or the version of jars which must be used to define the distribution -->
	<macrodef name="loadProperties" description="Load properties where are defined, for example, the qualifier or the version of jars which must be used to define the distribution">
		<attribute name="name" />
		<sequential>
			<!-- Check file existence -->
			<fail message="The properties file ${basedir}/${build.config}/@{name}.properties does not exists">
				<condition>
					<not>
						<available file="${basedir}/${build.config}/@{name}.properties" />
					</not>
				</condition>
			</fail>

			<property file="${basedir}/${build.config}/@{name}.properties" />
		</sequential>
	</macrodef>

	<!-- Generates the name of the release and display an information message -->
	<macrodef name="createPrometheeReleaseName">
		<sequential>
			<createReleaseName name="${app.name}" version="${app.version}" qualifier="${app.qualifier}" />
		</sequential>
	</macrodef>

	<macrodef name="createReleaseName">
		<attribute name="name" />
		<attribute name="version" />
		<attribute name="qualifier" />
		<attribute name="copyright" default="Artenum" />
		<attribute name="copyright-year" default="2002" />
		<attribute name="copyright-website" default="http://www.artenum.com" />
		<sequential>

			<!-- Generate the release name -->
			<condition property="release.name" value="@{name}-@{version}@{qualifier}${build.time}">
				<contains string="@{qualifier}" substring="-" />
			</condition>
			<condition property="release.name" value="@{name}-@{version}">
				<not>
					<contains string="@{qualifier}" substring="-" />
				</not>
			</condition>

			<!-- Define useful property for later -->
			<property name="build.release.dir" value="${basedir}/${build}/${release.name}" />


			<!-- Message to user -->
			<echo>###############################################################</echo>
			<echo>Deployment of @{name}</echo>
			<echo />
			<echo>---------------------------------------------------------------</echo>
			<echo>${release.name}</echo>
			<echo>---------------------------------------------------------------</echo>
			<echo />
			<echo>Copyright @{copyright} @{copyright-year}-${year}</echo>
			<echo>@{copyright-website}</echo>
			<echo>###############################################################</echo>
			<echo />
			<echo>Build system information:</echo>
			<echo />
			<echo>Architecture:     ${os.arch}</echo>
			<echo>Operating system: ${os.name}</echo>
			<echo>OS version:       ${os.version}</echo>
			<echo />
			<echo>Build performed with:</echo>
			<echo />
			<echo>Java runtime:     ${java.runtime.name}</echo>
			<echo>Java version:     ${java.runtime.version}</echo>
			<echo />
			<echo>Build time:       ${build.time}</echo>
			<echo />
		</sequential>
	</macrodef>

	<!-- Create the directories of the distribution -->
	<macrodef name="buildDistributionDirectories">
		<element name="additional-directories" optional="true" implicit="true" description="Put in this element the additional directories you want to create." />
		<sequential>
			<clean />

			<echo>###############################################################</echo>
			<echo>                Building distribution directories              </echo>
			<echo>###############################################################</echo>

			<!-- Creating the common distribution package -->
			<mkdir dir="${build.release.dir}/${data}" />
			<mkdir dir="${build.release.dir}/${dependencies.main}" />
			<mkdir dir="${build.release.dir}/${dependencies.thirdparty}" />
			<mkdir dir="${build.release.dir}/${documentation}" />
			<mkdir dir="${build.release.dir}/${resources}" />
			<mkdir dir="${build.release.dir}/lib" />
			<mkdir dir="${build.release.dir}/plugins" />
			<mkdir dir="${build.release.dir}/samples" />
			<mkdir dir="${build.release.dir}/scripts" />
			<additional-directories />

			<echo />
		</sequential>
	</macrodef>

	<!-- Copy the bundles that must be embedded in the distribution -->
	<macrodef name="copyBundles">
		<attribute name="bundles" />
		<sequential>
			<echo>###############################################################</echo>
			<echo>                      Copying main bundles                     </echo>
			<echo>###############################################################</echo>
			<condition property="repository" value="${env.USER_HOME}/.m2/repository" else="${user.home}/.m2/repository">
				<isset property="env.USER_HOME" />
			</condition>
			<loadFileList property="internal.bundles" path="${basedir}/${build.config}/@{bundles}" />
			<for param="line" list="${internal.bundles}" delimiter="${line.separator}">
				<sequential>
					<process-jar jarFile="@{line}" />
					<copy todir="${build.release.dir}/lib" file="@{line}" />
				</sequential>
			</for>

			<echo />
		</sequential>
	</macrodef>

	<!-- Copy the exemple data that will be embedded in the distribution -->
	<macrodef name="copyData">
		<sequential>
			<echo>###############################################################</echo>
			<echo>                    Filling data directory                     </echo>
			<echo>###############################################################</echo>

			<copyDirectory name="${data}" />
			<for param="file">
				<path>
					<fileset dir="${build.release.dir}/${data}" includes="*.zip" />
				</path>
				<sequential>
					<unzip dest="${build.release.dir}/${data}" src="@{file}" />
					<delete file="@{file}" />
				</sequential>
			</for>

			<echo />
		</sequential>
	</macrodef>

	<!-- Copy the documentation that will be embedded in the distribution -->
	<macrodef name="copyDocumentation">
		<element name="additional-exclude" optional="true" implicit="true" />
		<sequential>
			<echo>###############################################################</echo>
			<echo>                Filling documentation directory                </echo>
			<echo>###############################################################</echo>
			<copyDirectory name="${documentation}">
				<exclude name="**/developer/**" />
				<additional-exclude />
			</copyDirectory>

			<echo />
		</sequential>
	</macrodef>

	<!-- Copy the main dependencies that must be embedded in the distribution -->
	<macrodef name="copyMainDependencies">
		<attribute name="bundles" />
		<sequential>
			<echo>###############################################################</echo>
			<echo>                    Copying main dependencies                  </echo>
			<echo>###############################################################</echo>

			<condition property="repository" value="${env.USER_HOME}/.m2/repository" else="${user.home}/.m2/repository">
				<isset property="env.USER_HOME" />
			</condition>

			<!-- Copy all jar files from source dependencies/main -->
			<copyDirectory name="${dependencies.main}" />

			<!-- Delete developer tools -->
			<delete>
				<fileset dir="${build.release.dir}/${dependencies.main}">
					<include name="org.apache.felix.gogo.command-*.jar" />
					<include name="org.apache.felix.gogo.runtime-*.jar" />
					<include name="org.apache.felix.gogo.shell-*.jar" />
					<include name="org.apache.felix.ipojo.arch.gogo-*.jar" />
				</fileset>
			</delete>

			<!-- Copy all jar files from the dependencies.list.txt -->
			<loadFileList property="external.libs" path="${basedir}/${build.config}/@{bundles}" />
			<for param="line" list="${external.libs}" delimiter="${line.separator}">
				<sequential>
					<process-jar jarFile="@{line}" />
					<copy todir="${build.release.dir}/dependencies/main" file="@{line}" />
				</sequential>
			</for>

			<echo />
		</sequential>
	</macrodef>


	<!-- Download plugins that must be embedded in the distribution -->
	<macrodef name="copyPlugins" description="Download plugins that must be embedded in the distribution">
		<attribute name="plugins" />
		<sequential>
			<echo>###############################################################</echo>
			<echo>                    Downloading Funz plugins                   </echo>
			<echo>###############################################################</echo>

			<!-- Copy embedded plugins directories -->
			<mkdir dir="${basedir}/${plugins}"/>
			<copy todir="${build.release.dir}/plugins">
				<fileset dir="${basedir}/${plugins}" />
			</copy>
			<mkdir dir="${basedir}/${scripts}"/>
			<copy todir="${build.release.dir}/scripts">
				<fileset dir="${basedir}/${scripts}" />
			</copy>
			<mkdir dir="${basedir}/${samples}"/>
			<copy todir="${build.release.dir}/samples">
				<fileset dir="${basedir}/${samples}" />
			</copy>
			<mkdir dir="${basedir}/${lib}"/>
			<copy todir="${build.release.dir}/lib">
				<fileset dir="${basedir}/${lib}" />
			</copy>

			<!-- Plugins from GitHub-->
			<if>
				<length file="${basedir}/${build.config}/@{plugins}" when="greater" length="1"/>
				<then>
					<!-- GitHub plugins list -->
					<loadFileList property="plugins.io" path="${basedir}/${build.config}/@{plugins}" />
					<for param="ioplugin" list="${plugins.io}" delimiter="${line.separator}">
						<sequential>
							<get src="@{ioplugin}" dest="${build.release.dir}/plugin.zip"/>
							<unzip dest="${build.release.dir}/" src="${build.release.dir}/plugin.zip" />
							<delete>
								<fileset dir="${build.release.dir}/">
									<include name="plugin.zip" />
								</fileset>
							</delete>
						</sequential>
					</for>
				</then>
			</if>
			<echo />

		</sequential>
	</macrodef>

	<!-- Copy the resources folder that must be embedded in the distribution -->
	<macrodef name="copyResources" description="Copy the resources folder that must be embedded in the distribution">
		<element name="additional-excludes" optional="true" implicit="true" />
		<sequential>
			<copyDirectory name="resources">
				<additional-excludes />
			</copyDirectory>
		</sequential>
	</macrodef>

	<!-- Copy text files used for the packaging -->
	<macrodef name="copyTextFiles" description="Copy text files used for the packaging">
		<attribute name="name" />
		<element name="additional-include" optional="true" implicit="true" />
		<sequential>
			<echo>###############################################################</echo>
			<echo>                      Copying text files                       </echo>
			<echo>###############################################################</echo>

			<copy todir="${build.release.dir}">
				<fileset dir="${basedir}">
					<include name="*@{name}.txt" />
				</fileset>
				<mapper type="glob" from="*@{name}.txt" to="*.txt" />
			</copy>

			<copy todir="${build.release.dir}">
				<fileset dir="${basedir}">
					<include name="THIRDPARTY.txt" />
					<include name="VERSION.txt" />
					<additional-include />
				</fileset>
			</copy>

			<echo />
		</sequential>
	</macrodef>

	<!-- Copy the given attribute to thirdparty dependencies folder that will be embedded in the distribution -->
	<macrodef name="copyToThirdpartyDependencies" description="Copy the given attribute to thirdparty dependencies folder that will be embedded in the distribution">
		<attribute name="dependency" />
		<sequential>
			<copyDirectory name="${dependencies.thirdparty}/@{dependency}" />
		</sequential>
	</macrodef>

	<!-- Dynamically generate quotas.hex from quotas.xml -->
	<macrodef name="generateQuotas">
		<sequential>
			<echo>###############################################################</echo>
			<echo>                      Generate quotas.hex                      </echo>
			<echo>###############################################################</echo>
			<get src="https://github.com/Funz/funz-core/raw/master/dist/lib/funz-core-${funz.version}.jar" dest="${build}/funz-core.jar"/>
			<get src="https://github.com/Funz/funz-client/raw/master/dist/lib/funz-client-${funz.version}.jar" dest="${build}/funz-client.jar"/>
			<java classname="org.funz.conf.Configuration">
				<classpath path="${build}/funz-client.jar" />
				<classpath path="${build}/funz-core.jar" />
				<jvmarg value="-Dcharset=ISO-8859-1" />
				<arg value="quotas.xml" />
				<redirector output="quotas.hex" />
			</java>
			<echo />
		</sequential>
	</macrodef>

	<!-- Dynamically generate the calculator-localhost.xml -->
	<macrodef name="generateCalculatorFile">
		<attribute name="os" />
		<attribute name="dir" />
		<sequential>
			<!-- Copy the template calculator-localhost -->
			<copy todir="@{dir}">
				<fileset dir="${basedir}/${build.config}">
					<include name="${calc.file}.tpl" />
				</fileset>
				<mapper type="glob" from="*.tpl" to="*.xml" />
			</copy>

			<!-- Compute which files to include: .sh for *nix and .bat for Windows -->
			<local name="filefilterbat"/>
			<condition property="filefilterbat" value="*.bat" >
				<contains string="@{os}" substring="win" casesensitive="false" />
			</condition>

			<!-- Loop through all relevant scripts -->
			<fileset id="filesref" dir="@{dir}/scripts/" includes="${filefilterbat}"/>
			<local name="files"/>
			<property name="files" refid="filesref"/>

			<for list="${files}" delimiter=";" param="file" >
				<sequential>

					<local name="code" />
					<propertyregex property="code"
		    	              input="@{file}"
		    	              regexp="(.*)\.(bat)+"
		    	              select="\1"
		    	              casesensitive="false" />

					<echo message="Adding code '${code}' with script '@{file}' to ${calc.file}.xml"/>
					<echo file="@{dir}/${calc.file}.xml" append="true" >  &lt;CODE name='${code}' command='.\scripts\@{file}' /&gt;
					</echo>
				</sequential>
			</for>

			<local name="filefiltersh"/>
			<condition property="filefiltersh" value="*.sh" >
				<not>
					<contains string="@{os}" substring="win" casesensitive="false" />
				</not>
			</condition>

			<!-- Loop through all relevant scripts -->
			<fileset id="filesref" dir="@{dir}/scripts/" includes="${filefiltersh}"/>
			<local name="files"/>
			<property name="files" refid="filesref"/>

			<for list="${files}" delimiter=";" param="file" >
				<sequential>

					<local name="code" />
					<propertyregex property="code"
		    	              input="@{file}"
		    	              regexp="(.*)\.(sh)+"
		    	              select="\1"
		    	              casesensitive="false" />

					<echo message="Adding code '${code}' with script '@{file}' to ${calc.file}.xml"/>
					<echo file="@{dir}/${calc.file}.xml" append="true" >  &lt;CODE name='${code}' command='./scripts/@{file}' /&gt;
					</echo>
				</sequential>
			</for>


			<echo file="@{dir}/${calc.file}.xml" append="true" >&lt;/CALCULATOR&gt;
</echo>

		</sequential>
	</macrodef>

	<!-- Remove unused files like .svn and macOS files -->
	<macrodef name="removeHiddenFiles" description="Remove unused files like .svn and Mac files">
		<element name="additional-actions" optional="true" implicit="true" />
		<sequential>
			<!-- Remove all .svn files -->
			<delete includeemptydirs="true">
				<fileset dir="${build.release.dir}" defaultexcludes="false">
					<include name="**/.svn/" />
				</fileset>
			</delete>

			<!-- Remove all macOS Files files -->
			<delete includeemptydirs="true">
				<fileset dir="${build.release.dir}" defaultexcludes="false">
					<include name="**/__MACOSX/" />
				</fileset>
			</delete>

			<additional-actions />
		</sequential>
	</macrodef>

	<!-- Install the flavour to the platform -->
	<macrodef name="createDistribution">
		<attribute name="platform" />
		<sequential>
			<echo>###############################################################</echo>
			<echo>               Creating distribution for @{platform}           </echo>
			<echo>###############################################################</echo>

			<mkdir dir="${basedir}/${distribution}/@{platform}" />

			<!-- Create a local variable that store the location of the distribution -->
			<local name="platform.dir" />
			<property name="platform.dir" value="${basedir}/${distribution}/@{platform}/${release.name}-@{platform}" />
			<mkdir dir="${platform.dir}" />

			<!-- Copies the cross-platform basis -->
			<copy todir="${platform.dir}">
				<fileset dir="${build.release.dir}" />
			</copy>

			<!-- Copies and configures the platform-specific calculator list -->
			<generateCalculatorFile os="@{platform}" dir="${platform.dir}"/>
			
			<!-- Copies and configures the platform-specific execution script -->
			<copy todir="${platform.dir}">
				<filterchain>
					<expandproperties />
				</filterchain>
				<fileset dir="${basedir}/${build.config}/scripts/@{platform}">
					<include name="*.sh" />
					<include name="*.bat" />
				</fileset>
			</copy>

			<!-- Unzip JRE -->
			<unzip dest="${platform.dir}/${dependencies.thirdparty}" src="${basedir}/${dependencies.thirdparty}/jre-${jre.version}/jre-${jre.version}-@{platform}.zip" />

			<echo />
		</sequential>
	</macrodef>

	<!-- Zip the app for platform -->
	<macrodef name="zipDistribution">
		<attribute name="platform" />
		<sequential>
			<echo>###############################################################</echo>
			<echo>               Zipping distribution for @{platform}</echo>
			<echo>###############################################################</echo>

			<!-- Create a local variable that store the location of the distribution -->
			<local name="platform.dir" />
			<property name="platform.dir" value="${basedir}/${distribution}/@{platform}/${release.name}-@{platform}" />

			<!-- Zipping -->
			<zip destfile="${platform.dir}.zip" duplicate="preserve">
				<zipfileset dir="${platform.dir}" includes="**/*.sh" filemode="755" />
				<zipfileset dir="${platform.dir}" includes="**/*.bat" filemode="755" />
				<zipfileset dir="${platform.dir}" includes="**/*.exe" filemode="755" />
				<zipfileset dir="${platform.dir}" includes="**/*.dll" filemode="755" />
				<zipfileset dir="${platform.dir}" includes="**/java" filemode="755" />
				<zipfileset dir="${platform.dir}" includes="**/gmsh" filemode="755" />
				<zipfileset dir="${platform.dir}" includes="**/jspawnhelper" filemode="755" />
				<zipfileset dir="${platform.dir}" includes="**/*" />
			</zip>

			<echo />
		</sequential>
	</macrodef>

	<!-- Extracts the content of the given platform into the multi-os distribution -->
	<macrodef name="fillMutiOsFromPlatform">
		<attribute name="platform" />
		<sequential>
			<!-- Create a local variable that store the location of the distribution -->
			<local name="platform.dir" />
			<property name="platform.dir" value="${basedir}/${distribution}/${multios.name}/${release.name}-${multios.name}" />

			<!-- Copies and configures the platform-specific execution script -->
			<copy todir="${platform.dir}">
				<filterchain>
					<expandproperties />
				</filterchain>
				<fileset dir="${basedir}/${build.config}/scripts/@{platform}">
					<include name="*.sh" />
				</fileset>
				<mapper type="glob" from="*.sh" to="*-@{platform}.sh" />
			</copy>
			<copy todir="${platform.dir}">
				<filterchain>
					<expandproperties />
				</filterchain>
				<fileset dir="${basedir}/${build.config}/scripts/@{platform}">
					<include name="*.bat" />
				</fileset>
				<mapper type="glob" from="*.bat" to="*-@{platform}.bat" />
			</copy>

			<!-- Copy previously unzipped JRE -->
			<copy todir="${platform.dir}/dependencies/thirdparty/jre-${jre.version}-@{platform}">
				<fileset dir="${basedir}/${distribution}/@{platform}/${release.name}-@{platform}/dependencies/thirdparty/jre-${jre.version}-@{platform}">
					<include name="**/*" />
				</fileset>
			</copy>

			<!-- Now the JRE is copied, we can remove the original dir -->
			<delete dir="${basedir}/${distribution}/@{platform}/${release.name}-@{platform}/" />

		</sequential>
	</macrodef>

	<!-- Install for all platforms -->
	<macrodef name="createDistributions">
		<sequential>
			<!-- Create distribution for each platform -->
			<createDistribution platform="linux64b" />
			<createDistribution platform="osx64b" />
			<createDistribution platform="win32b" />
			<createDistribution platform="win64b" />

			<!-- Zip distribution for each platform -->
			<zipDistribution platform="linux64b" />
			<zipDistribution platform="osx64b" />
			<zipDistribution platform="win32b" />
			<zipDistribution platform="win64b" />

			<!-- Create multiOd distribution-->
			<echo>###############################################################</echo>
			<echo>               Creating multi-OS distribution                  </echo>
			<echo>###############################################################</echo>
			<mkdir dir="${basedir}/${distribution}/${multios.name}" />

			<!-- Copies the cross-platform basis -->
			<copy todir="${basedir}/${distribution}/${multios.name}/${release.name}-${multios.name}">
				<fileset dir="${build.release.dir}" />
			</copy>

			<!-- Copies the platform dependant ressources -->
			<fillMutiOsFromPlatform platform="linux64b"/>
			<fillMutiOsFromPlatform platform="osx64b"/>
			<fillMutiOsFromPlatform platform="win32b"/>
			<fillMutiOsFromPlatform platform="win64b"/>

			<!-- Zip and delete -->
			<zipDistribution platform="all" />
			<delete dir="${basedir}/${distribution}/${multios.name}/${release.name}-${multios.name}" />
		</sequential>
	</macrodef>

	<!-- Change the permission of executable files (.sh, .bat files for example) -->
	<macrodef name="makeScriptsExecutable" description="Change the permission of executable files (.sh, .bat files for example)">
		<element name="additional-files" optional="true" implicit="true" />
		<sequential>
			<echo>###############################################################</echo>
			<echo>                    Make scripts executable                   </echo>
			<echo>###############################################################</echo>
			<chmod perm="+x">
				<fileset dir="${basedir}/${distribution}">
					<include name="**/*.sh" />
					<include name="**/*.bat" />
					<include name="**/*.exe" />
					<include name="**/*.dll" />
					<include name="**/java" />
					<include name="**/gmsh" />
					<include name="**/jspawnhelper" />
				</fileset>
			</chmod>
		</sequential>
	</macrodef>

	<!-- Load a txt file list into properties -->
	<macrodef name="loadFileList">
		<attribute name="path" />
		<attribute name="property" />
		<sequential>
			<!-- Check file existence -->
			<fail message="The properties file @{path} does not exists">
				<condition>
					<not>
						<available file="@{path}" />
					</not>
				</condition>
			</fail>

			<loadfile property="@{property}" srcFile="@{path}">
				<filterchain>
					<expandproperties />
				</filterchain>
			</loadfile>
		</sequential>
	</macrodef>

	<macrodef name="process-jar">
		<attribute name="jarFile" />
		<sequential>
			<propertyregex property="@{jarFile}.name" input="@{jarFile}" regexp="[^/]+$" select="\0" />

			<loadproperties prefix="@{jarFile}.name">
				<!-- Load the manifest entries -->
				<zipentry zipfile="@{jarFile}" name="META-INF/MANIFEST.MF" />
			</loadproperties>

			<echo file="VERSION.txt" append="true">##############################################################${line.separator}</echo>
			<echo file="VERSION.txt" append="true">${@{jarFile}.name}:${line.separator}</echo>
			<echo file="VERSION.txt" append="true">Built-by: ${@{jarFile}.name.Built-By}${line.separator}</echo>

			<echo file="VERSION.txt" append="true">SVN revision: ${@{jarFile}.name.Implementation-Build}${line.separator}</echo>
			<echo file="VERSION.txt" append="true">${line.separator}</echo>
		</sequential>
	</macrodef>

	<!-- Copy the folder to embed it into the distribution -->
	<macrodef name="copyDirectory" description="Copy the folder to embed it into the distribution">
		<attribute name="name" />
		<element name="additional-exclude" optional="true" implicit="true" />
		<sequential>
			<copy todir="${build.release.dir}/@{name}">
				<fileset dir="${basedir}/@{name}">
					<additional-exclude />
				</fileset>
			</copy>
		</sequential>
	</macrodef>

</project>
