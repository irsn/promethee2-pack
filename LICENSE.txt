Software License Agreement Promethee


Grant of License

IRSN (hereafter identified as the Licensor) grants to the Licensee, a free of charge, personal, non-transferable, non-exclusive license to use the Promethee computer code, its associated data files and  libraries, interface software, and related documentation (hereafter referred to as the Software). The present License is granted for a given version of the Software and expressly excludes any of its previous and or future versions.
The Licensor reserves all rights not expressly granted to the Licensee in this agreement, and the Licensee, by accepting this agreement, agrees to comply with these terms and conditions.


Title and Ownership

The Licensor retains title and ownership rights of the Software and all subsequent copies of the Software, regardless of the form or media in or on which the original and other copies may exist.
The Licensee acknowledges the Licensor’s proprietary rights in the Software and agrees to reproduce all copyright notices supplied by the Licensor on all copies of the Software.
The Licensee will cite the Software on any publication of scientific results based on the Software.


Limitation of License and Restrictions

The Licensee may not modify, adapt, translate, reverse engineer, decompile, disassemble, or create derivative works based on the Software without the prior written consent of the Licensor.
The Software may not be transferred without the prior written consent of the Licensor. Any authorised transfer of the Software shall be bound by the terms and conditions of this Agreement.


Termination

This License is effective until terminated. This License will terminate automatically without notice from the Licensor if the Licensee fails to comply with or observe any of the provisions in this License.


Disclaimer

This Software and accompanying documents are licensed "as is". Neither the Licensor nor persons acting on their behalf make any representations or extend express or implied warranties: (a) with respect to the merchantability, accuracy, completeness, or usefulness of any services, materials, or information furnished hereunder, (b) that the use of any such services, materials, or information will not infringe privately owned rights, (c) that the services, materials, or information furnished hereunder will not result in injury or damage when used for any purpose, (d) that the services, materials, or information furnished hereunder will accomplish the intended results or are safe for any purpose.


Limitation of liabilities

The Licensor and the authors of the Software shall have no liability for direct, indirect, special, incidental, consequential, exemplary, or punitive damages of any character including, without limitation, procurement of substitute goods or services, loss of use, data or profits, or business interruption, however caused and on any theory of contract, warranty, tort (including negligence), product liability or otherwise, arising in any way out of the use of the Software, even if advised of the possibility of such damages.


Applicable Law - Litigation

The present License shall be construed in accordance with and governed by the substantial laws of France.
Any dispute between the Parties in relation to the present Agreement, which cannot be settled amicably within one (1) month from the occurrence of such dispute, shall be submitted exclusively to the competent courts of Nanterre, France.